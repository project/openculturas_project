<?php
// phpcs:ignoreFile

// More details in web/sites/default/default.settings.php.

$settings['file_public_path'] = 'sites/default/files';
$settings['file_temp_path'] = '../files/temp';
$settings['file_private_path'] = '../files/private';
$settings['config_sync_directory'] =  '../config/sync';
$config['locale.settings']['translation']['path'] = '../files/translations';

$settings['config_exclude_modules'] = [
  'devel',
  'config_inspector'
];

$settings['update_free_access'] = FALSE; // Access control for update.php script.

// The default list of directories that will be ignored by Drupal's file API.
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

$settings['entity_update_batch_size'] = 50; // The default number of entities to update in a batch process.
$settings['entity_update_backup'] = TRUE; // Entity update backup.

// Automatically generated include for settings managed by ddev.
if (file_exists($app_root . '/' . $site_path . '/settings.ddev.php') && getenv('IS_DDEV_PROJECT') == 'true') {
  include $app_root . '/' . $site_path . '/settings.ddev.php';
}
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}
$databases['default']['default']['init_commands'] = [
  'isolation_level' => 'SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED',
];
