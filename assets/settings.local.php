<?php
// phpcs:ignoreFile

assert_options(ASSERT_ACTIVE, TRUE);
assert_options(ASSERT_EXCEPTION, TRUE);

// Enable local development services.
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';

$settings['cache']['bins']['default'] = 'cache.backend.null'; // Disable the default cache.
$settings['cache']['bins']['render'] = 'cache.backend.null'; // Disable the render cache.
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null'; // Disable Dynamic Page Cache.
$settings['cache']['bins']['page'] = 'cache.backend.null'; // Disable Internal Page Cache.

// Disable CSS and JS aggregation.
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

$settings['trusted_host_patterns'] = [
  '.'
];

// Do not set web/sites/default to readonly.
$settings['skip_permissions_hardening'] = TRUE;

// Show all error messages, with backtrace information.
$config['system.logging']['error_level'] = ERROR_REPORTING_DISPLAY_VERBOSE;
